init_collapse_inlines = ->
    collapsible_inlines = $('.collapse_inline h2')
    # init may run more than once,
    # ensure we only have one click event handler
    collapsible_inlines.off 'click'
    collapsible_inlines.siblings().hide()
    collapsible_inlines.addClass 'closed'
    collapsible_inlines.on 'click', ->
        $(this).siblings().slideToggle(250)
        $(this).toggleClass 'closed'

contentblock_init_handlers.push init_collapse_inlines
