###! comparison helpers ###

Handlebars.registerHelper 'ifCond', (v1, operator, v2, options)->
    switch operator
        when '==', '==='
            return if v1 is v2 then options.fn @ else options.inverse @
        when '<'
            return if v1 < v2 then options.fn @ else options.inverse @
        when '<='
            return if v1 <= v2 then options.fn @ else options.inverse @
        when '>'
            return if v1 > v2 then options.fn @ else options.inverse @
        when '>='
            return if v1 >= v2 then options.fn @ else options.inverse @
        when '&&'
            return if v1 && v2 then options.fn @ else options.inverse @
        when '||'
            return if v1 || v2 then options.fn @ else options.inverse @
        else
            return options.inverse @

countdownClock = (cd) ->
  if cd.length
    countDays = cd.find('.countDays')
    countHours = cd.find('.countHours')
    countMinutes = cd.find('.countMinutes')
    countSeconds = cd.find('.countSeconds')
    countDownDate = new Date(cd.data('countdown-dt')).getTime()
    # Update the count down every 1 second
    x = setInterval(->
      # Get todays date and time
      now = (new Date).getTime()
      # Find the distance between now and the count down date
      distance = countDownDate - now
      # Time calculations for days, hours, minutes and seconds
      days = Math.floor(distance / (1000 * 60 * 60 * 24))
      hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))
      minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
      seconds = Math.floor((distance % (1000 * 60)) / 1000)
      # Insert HTML
      cd.find('.countDays').html days
      cd.find('.countHours').html hours
      cd.find('.countMinutes').html minutes
      cd.find('.countSeconds').html seconds
      # If the count down is over, write some text 
      if distance < 0
        clearInterval x
        $('.wrap-countdown').find('.time-titles').get(0).innerHTML = ''
        cd.get(0).innerHTML = 'Happening Now'
      return
    , 1000)
  return

cd = $('.countdowntimer')
countdownClock cd


storageAvailable = (type) ->
    storage
    try
        storage = window[type]
        x = '__storage_test__'
        storage.setItem(x, x)
        storage.removeItem(x)
        true
    catch e
        e instanceof DOMException && (
            #  everything except Firefox
            e.code == 22 ||
            # // Firefox
            e.code == 1014 ||
            # // test name field too, because code might not be present
            # // everything except Firefox
            e.name == 'QuotaExceededError' ||
            # // Firefox
            e.name == 'NS_ERROR_DOM_QUOTA_REACHED') &&
            # // acknowledge QuotaExceededError only if there's something already stored
            (storage && storage.length != 0)

$(document).foundation({
    interchange: {
        named_queries: {
            middling: 'only screen and (min-width: 900px)'
        }
    }
})

$(document).scroll( () ->
    scroll_top = $(document).scrollTop()
    if scroll_top <= 300
        $('.floating-return').fadeOut()
    else
        $('.floating-return').fadeIn()
)

$(".floating-return").on "click", ->
  $('html, body').animate({
    scrollTop: 0
    }, 800
  )

debug = false

default_headers = {
    "Accept": "application/json"
    "Authorization": "Token 1ce2d13c0b900d35897fa6bdae992310e3433959"
}

rest_urls = {
    dept_info: (context) -> "https://dir-data.shc.edu/api/group/#{context.code}/.json",
    group_contact_info: (context) -> "https://dir-data.shc.edu/api/emps/group/#{context.code}/.json?#{context.qs}"
}

decipher = (coded) ->
    link = ""
    for i in [0..coded.length] by 2
        chunk = coded[i..i+1]
        link = link + String.fromCharCode(parseInt(chunk, 16))
    return link

unmunge_emails = (selector='a.memail') ->
    $(selector).each((idx, val) ->
        email = decipher($(this).data('coded'))
        $(this).attr('href', 'mailto:' + email)
        $(this).children(".email").text(email)
    )

can_play_video = () ->
    video_types = [
        'video/webm; codecs="vp8.0, vorbis"'
        'video/ogg; codecs="theora, vorbis"'
        'video/mp4; codecs="avc1.4D401E, mp4a.40.2"'
    ]
    vid = document.createElement('video')
    playsVideo = false
    for type in video_types
        if vid.canPlayType(type) is 'probably'
            return true

slugify = (text) ->
  return text.toString().toLowerCase()
    .replace(/\s+/g, '-')          # Replace spaces with -
    .replace(/[^\w\-]+/g, '')      # Remove all non-word chars
    .replace(/\-\-+/g, '-')        # Replace multiple - with single -
    .replace(/^-+/, '')            # Trim - from start of text
    .replace(/-+$/, '')            # Trim - from end of text

# TODO: Any way to generalize these next two functions a little more?
renderJSONData = (context, data, templ_id, partials='', callbacks='') ->
    # register partials if provided
    if partials
        for partial in partials
            Handlebars.registerPartial(partial, Handlebars.compile($("##{partial}").html()))
    # compile and render the main template
    template = hbt[templ_id]
    # console.log templ_id
    return if not template

    # template = Handlebars.compile(html_template)
    rendered = template(data)
    # insert the rendered data into the page
    context.html(rendered)
    # run callbacks if needed
    if callbacks
        callback() for callback in callbacks

renderTemplateFromAJAX = (template, rest_code, target_id, callback) ->
    code = $(".#{target_id}").data("code")
    qs = $(".#{target_id}").data("qs")
    context = { code: code, qs: qs }
    rest_url = rest_urls[rest_code](context)

    ajax_settings = {
        "url": rest_url
        "headers": default_headers
    }
    $.ajax ajax_settings
        .done (data, status) ->
            # console.log "finished rendering dept info"
            if data
                target = $(".#{target_id}")
                if target
                    target.html template(data)
                if callback? and callback
                    callback()

# this needs to be a function so we can pass it as a callback
foundationReflow = ->
    $(document).foundation('reflow')

# retrigger initializations after interchange has loaded
$(document).on 'replace', '.interchange-main-nav', (e, new_path, original_path) ->
    foundationReflow()
    init_searchbox()
$(document).on 'replace', '.left-off-canvas-menu', (e, new_path, original_path) ->
    foundationReflow()
    init_searchbox()

getQS = ->
    queries = {}
    $.each(document.location.search.substr(1).split('&'), (c,q) ->
        if q
            i = q.split '='
            queries[i[0].toString()] = i[1].toString()
        )
    queries

init_searchbox = ->
    $("a.show-search-button").on "click", ->
        self = $(this)
        $(this).hide 0
        $(".search-button").show 0
        $(".search input").fadeIn 300, ->
            $(this).select()

    # add search query back to the search bar after page loads
    qs = getQS()?["q"]
    if qs
        $(".search input").val(decodeURI(qs))

show_modal = (context) ->
    context.foundation('reveal', 'open')
    if store.enabled
        store.set "showgeneralcontactmodal", 0

# show_hometakeover_modal = (context) ->
#     context.foundation('reveal', 'open')
#     if store.enabled
#         store.set "showhometakeovercontactmodal-wtth-03-2020", 0

flag_single_links = (context) ->
    $("p:has(a)", context).filter ->
        return $(this).contents().length is 1
    .children('a').addClass "singleLink"

search_param_map = {
    "ug": "undergraduate"
    "grad": "graduate"
    "cs": "continuing studies adult"
}

quotes = [
    {
        quote: "<p>Spring Hill has challenged me in the best way. I’ve grown as a person, learned how to effectively manage my time, and most importantly, I’ve learned who I am while being here.</p>"
        attribution: "Morgan Davis<br>SHC Class of 2017<br>Cellular and Molecular Biology Major"
    }
    {
        quote: "<p>The most valuable community service I’ve done thus far has been my commitment to the young men at the Strickland Youth Detention Center. Mentoring them has helped me see the need for positive male role models in the lives of young men all across Mobile.</p>"
        attribution: "Derrick Robbins<br>SHC Class of 2017<br>Sociology Major"
    }
    {
        quote: "<p>Spring Hill made me more confident in my abilities because I learned I could do all of these things. I could make friends. I could join activities. I could talk about and give tours of the school. I could travel on my own.</p>"
        attribution: "Kathleen Fasold<br>SHC Class of 2017<br>English Major"
    }
    {
        quote: "<p>Spring Hill has allowed me to see how I can make service a part of my everyday life and has given me the confidence to make that a reality.</p>"
        attribution: "Claire Oswald<br>SHC Class of 2017<br>Health Science Major"
    }
    {
        quote: "<p>The spirit of Spring Hill lives at the core of this community. It works within us and unifies eternally. It has molded us into the heroic leaders that this world desperately needs right now.</p>"
        attribution: "John Michael Rogers<br>SHC Class of 2017<br>Senior Class Commencement Orator"
    }
]

$ ->
    
    if debug
        $('body').prepend '<div class="panel callout">DEBUGGING!</div>'
    
    
    if $('.quotes').length
        $('.quotes').html hbt["templates/partials/person/quote.handlebars"]({quotes:quotes})

        $('.quotes').slick({
            speed: 700
            autoplay: true
            autoplaySpeed: 8000
            fade: true
            arrows: false
            dots: true
            })
    
    if $('video').length and not can_play_video()
        $('video').hide()
        $('.video-alt').show()
    
    doVideo = () ->
        sliderEl.find('.banner.slick-slide video').each () ->
            $(this).get(0).pause()
        activeVideo = sliderEl.find('.banner.slick-slide.slick-active video')
        if activeVideo.length
            activeVideo.get(0).play()

    
    sliderEl = $('.wrap-home-banner')
    if sliderEl.length
        $('.wrap-home-banner').on('init', (e, slick) ->
            doVideo()
        )
        $('.wrap-home-banner').on('afterChange', (e, slick) ->
            doVideo()
        )
        sliderEl.slick(
            {
                easing: 'swing'
                speed: 1500
                fade: false
                arrows: true
                dots: true
            }
        )

    flag_single_links $('.page-content, #main-content')

    four_oh_four = $("form.not_found .search")
    if four_oh_four.length
        pathname = window.location.pathname
        # see if path starts with "/search"
        if not /^\/search/.test(pathname)
            pieces = pathname.split(/[\-\_\/]/)[1..-2]
            new_pieces = []
            for piece in pieces
                if search_param_map[piece]
                    new_pieces.push search_param_map[piece]
                else
                    new_pieces.push piece
            $("#q-body").val(new_pieces.join(" "))

    # initialize variable to hide popover modal on subsequent viewings
    if store.enabled
        if not store.get("showgeneralcontactmodal")?
            store.set "showgeneralcontactmodal", 1
        # if not store.get("showhometakeovercontactmodal-wtth-03-2020")?
            # store.set "showhometakeovercontactmodal-wtth-03-2020", 1

    # show the popover modal if user hasn't seen it
    ms = $(".reveal-modal").data("show-on-delay")
    if store.enabled and ms and store.get("showgeneralcontactmodal") is 1
        setTimeout show_modal, ms, $(".reveal-modal")
    # if store.enabled and store.get("showhometakeovercontactmodal-wtth-03-2020") is 1
    #     show_hometakeover_modal($("#homeTakeoverModal"))
    
    # show proper menu level when off-canvas menu opened
    $(document).on 'open.fndtn.offcanvas', '[data-offcanvas]', ->
        setTimeout ->
            $(".left-off-canvas-menu li.active.has-submenu > ul[class~='left-submenu']").addClass("move-right")
        , 200

    if not debug
        # Add event tracking for Analytics
        $('.prod.anon #button-open-acad-info-req-form').on 'click', ->
            ga('send', 'event', 'Forms', 'open', 'Academic request more info')
        # Track clicks on UG Degrees panel from Freshman landing page
        $('.opt-ug-degrees a.button').on 'click', ->
            ga('send', 'event', 'Links', 'click', 'UG Degrees on Freshman landing')
        $('.badger-day-button').on 'click', ->
            ga('send', 'event', 'Banner', 'click', 'Badger Day Web Campaign')
        $('.wrap-video-mod .video-container img').on 'click', ->
            ga('send', 'event', 'Video', 'play', 'Freshmen timelapse')
        
        $('.apply-link.freshmen').on 'click', ->
            ga('send', 'event', 'Links', 'click', 'Freshman App - from ' + window.location.href)
        $('.apply-link.transfer').on 'click', ->
            ga('send', 'event', 'Links', 'click', 'Transfer App - from ' + window.location.href)
        $('.apply-link.grad').on 'click', ->
            ga('send', 'event', 'Links', 'click', 'Graduate App - from ' + window.location.href)
        $('.apply-link.cs').on 'click', ->
            ga('send', 'event', 'Links', 'click', 'Continuing Studies App - from ' + window.location.href)
        $('.apply-link.readmit').on 'click', ->
            ga('send', 'event', 'Links', 'click', 'Readmit App - from ' + window.location.href)

    $('.social-menu .snapchat').closest('a').attr({'href': '#', 'data-reveal-id': 'snapModal'})
    if $(".image-carousel").length
      $(".image-carousel").slick({
        lazyLoad: 'progressive'
        easing: 'swing'
        speed: 500
        fade: false
        arrows: true
        dots: true
        })

      $(".action-open-gallery").on 'click', ->
        idx = $(this).data('image-index')
        $(".image-carousel").slick 'slickGoTo', idx
        $(document).foundation 'reveal', 'reflow'

    jump_selector = ".jump"
    if $(".do-jumplinks").length and $(jump_selector).length

      jump_buffer = 72 + 10 # top menu height + padding

      $(".do-jumplinks").each ->
        targetcat = $(this).data 'category'
        jumplink_elements = []
        linkgroup = $('<ul></ul>')
        if targetcat
          selector = "#{jump_selector}.#{targetcat}"
        $(selector).each (idx) ->
          self = $(@)
          title = self.data("short-title") or self.text()
          # append cat to id to avoid namespace conflicts
          id = self.attr("id") or slugify("#{targetcat}-#{title}")
          jump_target = $("<a class=\"jump-target\" id=\"#{ id }\"></a>")
          self.before jump_target

          jump_link = $("<li><a href=\"##{ id }\">#{ title }</a></li>")
          jump_link.on('click', (e) ->
            e.preventDefault()
            top = jump_target.offset().top - jump_buffer
            $('html, body').animate({
              scrollTop: top
              }, 800
            )
            # make sure this location is linkable
            loc = window.location.href.split("#")[0]
            history.pushState({}, "", "#{loc}##{id}")
          )
          linkgroup.append jump_link
        $(this).html linkgroup

    # tag external links as such
    fake_internal_url_patterns = [
        /^https?:\/\/s3\.amazonaws\.com\//
        /^https?:\/\/www\.shc\.edu/
    ]
    $('.content a[href^="http"]').each ->
        self = $(this)
        flag_external = true
        for pattern in fake_internal_url_patterns
            if pattern.test self.attr('href')
                flag_external = false
                break
        if flag_external
            self.addClass 'new-window'
            # add an icon unless the link has an image as a child
            if  self.not(':has(>img)').length
                self.addClass 'has-icon'

    # open tagged links in new window
    $("a.new-window").attr("target", "_blank")

    # render departmental data in client-side handlebars template
    if $(".rendered-dept-info").length
        renderTemplateFromAJAX(hbt["templates/partials/academics/dept-info.handlebars"], "dept_info", "rendered-dept-info")
    # render departmental quicklink in client-side handlebars template
    if $(".rendered-dept-quicklink").length
        renderTemplateFromAJAX(hbt["templates/partials/academics/dept-quicklink.handlebars"], "dept_info", "rendered-dept-quicklink")
    # render group contact info in client-side handlebars template
    if $(".rendered-group-contacts").length
        renderTemplateFromAJAX(hbt["templates/partials/contact/group-contacts.handlebars"], "group_contact_info", "rendered-group-contacts", callback=unmunge_emails)

    # add a class for selectively styling page blocks
    # TODO: get this completely into CSS?
    blocks = $(".sidebar .block")
    blocks.each (idx) ->
        is_header_block = $(this).children().length == 1 and $(this).children("h1, h2, h3").filter(":first").length
        if idx+1 isnt blocks.length and not is_header_block
            $(this).addClass("blocky")

    $("input[required]").prev("label").addClass("required")

    # process "more" tags in long lists
    # HTML looks like this (usually generated from textile):
    # <ul>
    #   <li></li>
    #   ...
    # </ul>
    # <hr class="more">
    # <ul>
    #   <li></li>
    #   ...
    # </ul>
    # The second ul will be replaced with a 'more' button linked
    # to a foundation dropdown
    $("br.more").each (idx)->
        self = $(this)
        id = "drop#{idx}"
        dropdown = $("<a href=\"#\" class=\"more button tiny\" data-dropdown=\"#{id}\">See All &raquo;</a>")
        self.prev("ul").attr("class", "less-list")
        self.next("ul").attr({"id": id, "class": "f-dropdown content more-list", "data-dropdown-content":""})
        self.replaceWith(dropdown)

    $("form #id_address").attr "rows", 4

    $('.page-content img, .entry-content img').each ->
        $(this).on 'load', ->
            if $(this).height() > $(this).width()
                $(this).addClass 'portrait'
            else
                $(this).addClass 'landscape'

    foundationReflow()
