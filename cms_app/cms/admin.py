from django.contrib.sites.models import Site
from django.contrib.redirects.models import Redirect
from django.contrib import messages
from django.conf import settings
from feincms.module.page.modeladmins import PageAdmin
from django.db import IntegrityError, transaction

from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.contrib import admin


def save_page(self, request, obj, form, change):

    site_id = settings.SITE_ID
    site = Site.objects.get(pk=site_id)
    old_path = obj.alias
    new_path = obj.get_absolute_url()

    redir = None
    if not old_path:
        dead_redirs = Redirect.objects.filter(new_path=new_path)
        if dead_redirs.count():
            messages.add_message(request,
                                 messages.WARNING,
                                 "Existing aliases pointing to this page have been deleted: %s" % [
                                     redir.old_path for redir in dead_redirs]
                                 )
            dead_redirs.delete()
        obj.save()

    else:
        try:
            # see if we have an exact match in existing aliases
            redir = Redirect.objects.get(old_path=old_path, new_path=new_path)
        except Redirect.DoesNotExist:
            pass
        if redir:
            obj.save()
            return
        else:
            try:
                # see if we are creating or updating an alias
                redir = Redirect.objects.get(new_path=new_path)
            except Redirect.DoesNotExist:
                redir = Redirect()

            redir.site = site
            redir.old_path = old_path
            redir.new_path = new_path
            try:
                with transaction.atomic():
                    redir.save()
                obj.save()
            except IntegrityError:
                messages.add_message(request,
                                     messages.ERROR,
                                     """The alias %s already points to another page in the system.
                                     The page was saved, but you will need to change the alias.""" % obj.alias
                                     )
                obj.alias = ''
                obj.save()


class CustomUserAdmin(UserAdmin):
    def __init__(self, *args, **kwargs):
        super(UserAdmin,self).__init__(*args, **kwargs)
        UserAdmin.list_editable = list(UserAdmin.list_editable) + ['is_staff']

admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
