# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('elephantblog', '0004_auto_20170927_1454'),
        ('page', '0009_auto_20180208_1012'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='newslistingcontent',
            name='category',
        ),
        migrations.AddField(
            model_name='newslistingcontent',
            name='categories',
            field=models.ManyToManyField(to='elephantblog.CategoryTranslation', verbose_name='category'),
            preserve_default=True,
        ),
    ]
