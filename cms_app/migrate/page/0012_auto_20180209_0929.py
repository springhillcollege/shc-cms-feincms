# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('page', '0011_auto_20180208_1125'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newslistingcontent',
            name='num_items',
            field=models.IntegerField(default=0, verbose_name='num items'),
            preserve_default=True,
        ),
    ]
