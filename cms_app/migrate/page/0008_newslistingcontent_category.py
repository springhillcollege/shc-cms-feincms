# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        # ('medialibrary', '0001_initial'),
        ('page', '0007_remove_newslistingcontent_category'),
    ]

    operations = [
        migrations.AddField(
            model_name='newslistingcontent',
            name='category',
            field=models.ForeignKey(related_name='+', default=1, verbose_name='category', to='medialibrary.Category'),
            preserve_default=False,
        ),
    ]
