# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import feincms.contrib.fields
import feincms.contrib.richtext
import feincms.module.medialibrary.fields
import feincms.module.extensions.datepublisher
import feincms.module.mixins
import feincms.extensions


class Migration(migrations.Migration):

    dependencies = [
        # ('forms', '0003_auto_20160831_1132'),
        ('feincms_links', '0002_auto_20160517_1415'),
        ('profiles', '0001_initial'),
        ('page_partials', '0002_pagepartial_slug'),
        ('medialibrary', '__first__'),
        ('testimonials', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AfforabilityCalcContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'affordability calculators',
                'db_table': 'page_page_afforabilitycalccontent',
                'verbose_name': 'affordability calculator',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ApplicationContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('parameters', feincms.contrib.fields.JSONField(null=True, editable=False)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('urlconf_path', models.CharField(max_length=100, verbose_name='application', choices=[('jobs.urls', 'Jobs application'), ('elephantblog.urls', 'Blog'), ('magazine.urls', 'Magazine')])),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'application contents',
                'db_table': 'page_page_applicationcontent',
                'verbose_name': 'application content',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CampusMapContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'map contents',
                'db_table': 'page_page_campusmapcontent',
                'verbose_name': 'map content',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CounselorMapContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'counselor maps',
                'db_table': 'page_page_counselormapcontent',
                'verbose_name': 'counselor map',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DirectoryListingContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('group_code', models.CharField(help_text='Use code "search" to crate a searchable directory not tied to a specific department.', max_length=6, verbose_name='group code')),
                ('query_options', models.CharField(help_text="if you don't know what this is, don't use it", max_length=100, verbose_name='query options', blank=True)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'directory listings',
                'db_table': 'page_page_directorylistingcontent',
                'verbose_name': 'directory listing',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FormContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('form', models.ForeignKey(verbose_name='Form', to='forms.Form')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'Forms',
                'db_table': 'page_page_formcontent',
                'verbose_name': 'Form',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='JumpLinksContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('layout', models.CharField(default='block-links', max_length=15, verbose_name='Layout', choices=[('block-links', 'block'), ('inline-links', 'inline')])),
                ('style', models.CharField(default='bullets', max_length=15, verbose_name='Link Style', choices=[('bullets', 'with bullets'), ('no-bullets', 'without bullets')])),
                ('category', models.SlugField(help_text='Can contain only letters, numbers, and hyphens', max_length=15, verbose_name='Link Class Category', blank=True)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'Jump Links Content',
                'db_table': 'page_page_jumplinkscontent',
                'verbose_name': 'Jump Links Content',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LinkContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('category', models.ForeignKey(blank=True, to='feincms_links.Category', help_text='Leave blank to list all categories.', null=True, verbose_name='category')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'link lists',
                'db_table': 'page_page_linkcontent',
                'verbose_name': 'link list',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MediaCategoryListingContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('layout', models.CharField(default='list-grid.html', max_length=25, choices=[('list-grid.html', 'Grid')])),
                ('reverse_sort', models.BooleanField()),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('category', models.ForeignKey(related_name='+', verbose_name='category', to='medialibrary.Category')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'media category listings',
                'db_table': 'page_page_mediacategorylistingcontent',
                'verbose_name': 'media category listing',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MediaFileContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('type', models.CharField(default='default', max_length=20, verbose_name='type', choices=[('default', 'default'), ('banner', 'banner'), ('captioned banner', 'banner w/ caption'), ('inline', 'inline'), ('captioned inline', 'inline w/ caption'), ('responsive', 'responsive'), ('captioned responsive', 'responsive w/ caption')])),
                ('mediafile', feincms.module.medialibrary.fields.MediaFileForeignKey(related_name='+', verbose_name='media file', to='medialibrary.MediaFile')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'media files',
                'db_table': 'page_page_mediafilecontent',
                'verbose_name': 'media file',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ModalFormContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('form_id', models.IntegerField(max_length=5, verbose_name='form id')),
                ('button_txt', models.CharField(max_length=100, verbose_name='button text', blank=True)),
                ('form_title', models.CharField(max_length=200, verbose_name='form title', blank=True)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'Modal Form Embeds',
                'db_table': 'page_page_modalformcontent',
                'verbose_name': 'Modal Form Embed',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='NewsListingContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('num_items', models.IntegerField(verbose_name='num items')),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'news listings',
                'db_table': 'page_page_newslistingcontent',
                'verbose_name': 'news listing',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('active', models.BooleanField(default=True, verbose_name='active')),
                ('title', models.CharField(help_text='This title is also used for navigation menu items.', max_length=200, verbose_name='title')),
                ('slug', models.SlugField(help_text='This is used to build the URL for this page', max_length=150, verbose_name='slug')),
                ('in_navigation', models.BooleanField(default=False, verbose_name='in navigation')),
                ('override_url', models.CharField(help_text="Override the target URL. Be sure to include slashes at the beginning and at the end if it is a local URL. This affects both the navigation and subpages' URLs.", max_length=255, verbose_name='override URL', blank=True)),
                ('redirect_to', models.CharField(help_text='Target URL for automatic redirects or the primary key of a page.', max_length=255, verbose_name='redirect to', blank=True)),
                ('_cached_url', models.CharField(default='', editable=False, max_length=255, blank=True, verbose_name='Cached URL', db_index=True)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('template_key', models.CharField(default='bricks_left', max_length=255, verbose_name='template', choices=[('bricks_left', 'Bricks with left sidebar'), ('single_col_stacked', 'Single Column'), ('finaid_page', 'Finaid Page'), ('sellsheet', 'For UG Academic Sellsheets'), ('landing', 'Landing Pages'), ('home', 'Site homepage')])),
                ('_ct_inventory', feincms.contrib.fields.JSONField(verbose_name='content types', null=True, editable=False, blank=True)),
                ('publication_date', models.DateTimeField(default=feincms.module.extensions.datepublisher.granular_now, verbose_name='publication date')),
                ('publication_end_date', models.DateTimeField(help_text='Leave empty if the entry should stay active forever.', null=True, verbose_name='publication end date', blank=True)),
                ('creation_date', models.DateTimeField(verbose_name='creation date', null=True, editable=False)),
                ('modification_date', models.DateTimeField(verbose_name='modification date', null=True, editable=False)),
                ('excerpt_text', models.TextField(verbose_name='text', blank=True)),
                ('featured', models.BooleanField(default=False, verbose_name='featured')),
                ('navigation_extension', models.CharField(help_text='Select the module providing subpages for this page if you need to customize the navigation.', max_length=200, null=True, verbose_name='navigation extension', blank=True)),
                ('_content_title', models.TextField(help_text='The first line is the main title, the following lines are subtitles.', verbose_name='content title', blank=True)),
                ('_page_title', models.CharField(help_text='Page title for browser window. Same as title by default. Must be 69 characters or fewer.', max_length=69, verbose_name='page title', blank=True)),
                ('navigation_group', models.CharField(default='default', max_length=20, verbose_name='navigation group', db_index=True, choices=[('default', 'Default'), ('meta', 'Meta'), ('subnav1', 'Subnav 1'), ('subnav2', 'Subnav 2'), ('subnav3', 'Subnav 3')])),
                ('fake_leaf', models.BooleanField(default=False, help_text='Treat this page as if it has no children', verbose_name='fake leaf')),
                ('excerpt_image', feincms.module.medialibrary.fields.MediaFileForeignKey(related_name='+', verbose_name='image', blank=True, to='medialibrary.MediaFile', null=True)),
                ('parent', models.ForeignKey(related_name='children', verbose_name='Parent', blank=True, to='page.Page', null=True)),
                ('related_pages', models.ManyToManyField(help_text='Select pages that should be listed as related content.', related_name='page_page_related', to='page.Page', blank=True)),
                ('symlinked_page', models.ForeignKey(related_name='page_page_symlinks', blank=True, to='page.Page', help_text='All content is inherited from this page if given.', null=True, verbose_name='symlinked page')),
                ('testimonials', models.ManyToManyField(help_text='Select testimonials that should be related to this page.', related_name='testimonials', null=True, to='testimonials.Testimonial', blank=True)),
            ],
            options={
                'ordering': ['tree_id', 'lft'],
                'verbose_name': 'page',
                'verbose_name_plural': 'pages',
            },
            bases=(models.Model, feincms.extensions.ExtensionsMixin, feincms.module.mixins.ContentModelMixin),
        ),
        migrations.CreateModel(
            name='PagePartialContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('parent', models.ForeignKey(related_name='pagepartialcontent_set', to='page.Page')),
                ('partial', models.ForeignKey(to='page_partials.PagePartial')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'page partial contents',
                'db_table': 'page_page_pagepartialcontent',
                'verbose_name': 'page partial content',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PageTeaserContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, verbose_name='title', blank=True)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('mediafile', feincms.module.medialibrary.fields.MediaFileForeignKey(related_name='+', verbose_name='mediafile', to='medialibrary.MediaFile')),
                ('page', models.ForeignKey(related_name='+', verbose_name='page', to='page.Page')),
                ('parent', models.ForeignKey(related_name='pageteasercontent_set', to='page.Page')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'page teasers',
                'db_table': 'page_page_pageteasercontent',
                'verbose_name': 'page teaser',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProfileContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.PositiveIntegerField(default=0, help_text='Leave blank to show all profiles.', verbose_name='number')),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('category', models.ForeignKey(blank=True, to='profiles.Category', help_text='Leave blank to list all categories.', null=True, verbose_name='category')),
                ('parent', models.ForeignKey(related_name='profilecontent_set', to='page.Page')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'profile content',
                'db_table': 'page_page_profilecontent',
                'verbose_name': 'profile content',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RawContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField(verbose_name='content', blank=True)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('parent', models.ForeignKey(related_name='rawcontent_set', to='page.Page')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'raw contents',
                'db_table': 'page_page_rawcontent',
                'verbose_name': 'raw content',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RichTextContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', feincms.contrib.richtext.RichTextField(verbose_name='text', blank=True)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('parent', models.ForeignKey(related_name='richtextcontent_set', to='page.Page')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'rich texts',
                'db_table': 'page_page_richtextcontent',
                'verbose_name': 'rich text',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SlideContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, verbose_name='title', blank=True)),
                ('subtitle', models.CharField(max_length=100, verbose_name='subtitle', blank=True)),
                ('area', models.CharField(default='50x50', max_length=10, verbose_name='preferred area if cropping', choices=[('50x50', 'center'), ('50x20', 'center / top'), ('20x50', 'left / center'), ('80x50', 'right / center'), ('50x80', 'center / bottom')])),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('mediafile', feincms.module.medialibrary.fields.MediaFileForeignKey(related_name='+', verbose_name='media file', to='medialibrary.MediaFile')),
                ('parent', models.ForeignKey(related_name='slidecontent_set', to='page.Page')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'slides',
                'db_table': 'page_page_slidecontent',
                'verbose_name': 'slide',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SubpageContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('style', models.CharField(default='default', max_length=20, verbose_name='style', choices=[('default', 'list'), ('simple', 'simple'), ('grid', 'grid')])),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('parent', models.ForeignKey(related_name='subpagecontent_set', to='page.Page')),
                ('start_page', models.ForeignKey(related_name='subpage_content', blank=True, to='page.Page', help_text='Leave blank to use current page', null=True)),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'subpages listings',
                'db_table': 'page_page_subpagecontent',
                'verbose_name': 'subpages listing',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TemplateContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('filename', models.CharField(max_length=100, verbose_name='template', choices=[('academic-audience-selector.html', 'academic-audience-selector.html'), ('admission-audience-selector.html', 'admission-audience-selector.html'), ('grad-menu.html', 'grad-menu.html'), ('grad_degrees.html', 'grad_degrees.html'), ('job_postings.html', 'job_postings.html'), ('news-teaser.html', 'news-teaser.html'), ('searchbox.html', 'searchbox.html'), ('searchresults.html', 'searchresults.html'), ('ug-majors-and-minors.html', 'ug-majors-and-minors.html')])),
                ('parent', models.ForeignKey(related_name='templatecontent_set', to='page.Page')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'template contents',
                'db_table': 'page_page_templatecontent',
                'verbose_name': 'template content',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TextilePageContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('show_on', models.DateTimeField(help_text='leave blank to publish now', null=True, blank=True)),
                ('hide_after', models.DateTimeField(help_text='leave blank for no expiration', null=True, blank=True)),
                ('content', models.TextField()),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('parent', models.ForeignKey(related_name='textilepagecontent_set', to='page.Page')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'textile page contents',
                'db_table': 'page_page_textilepagecontent',
                'verbose_name': 'textile page content',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='newslistingcontent',
            name='parent',
            field=models.ForeignKey(related_name='newslistingcontent_set', to='page.Page'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='modalformcontent',
            name='parent',
            field=models.ForeignKey(related_name='modalformcontent_set', to='page.Page'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='mediafilecontent',
            name='parent',
            field=models.ForeignKey(related_name='mediafilecontent_set', to='page.Page'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='mediacategorylistingcontent',
            name='parent',
            field=models.ForeignKey(related_name='mediacategorylistingcontent_set', to='page.Page'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='linkcontent',
            name='parent',
            field=models.ForeignKey(related_name='linkcontent_set', to='page.Page'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='jumplinkscontent',
            name='parent',
            field=models.ForeignKey(related_name='jumplinkscontent_set', to='page.Page'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='formcontent',
            name='parent',
            field=models.ForeignKey(related_name='formcontent_set', to='page.Page'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='directorylistingcontent',
            name='parent',
            field=models.ForeignKey(related_name='directorylistingcontent_set', to='page.Page'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='counselormapcontent',
            name='parent',
            field=models.ForeignKey(related_name='counselormapcontent_set', to='page.Page'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='campusmapcontent',
            name='parent',
            field=models.ForeignKey(related_name='campusmapcontent_set', to='page.Page'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='applicationcontent',
            name='parent',
            field=models.ForeignKey(related_name='applicationcontent_set', to='page.Page'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='afforabilitycalccontent',
            name='parent',
            field=models.ForeignKey(related_name='afforabilitycalccontent_set', to='page.Page'),
            preserve_default=True,
        ),
    ]
