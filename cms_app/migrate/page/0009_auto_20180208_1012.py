# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('page', '0008_newslistingcontent_category'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newslistingcontent',
            name='category',
            field=models.ForeignKey(related_name='+', verbose_name='category', to='elephantblog.CategoryTranslation'),
            preserve_default=True,
        ),
    ]
