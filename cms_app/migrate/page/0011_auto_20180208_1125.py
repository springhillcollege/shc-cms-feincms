# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('page', '0010_auto_20180208_1115'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newslistingcontent',
            name='categories',
            field=models.ManyToManyField(to='elephantblog.Category', verbose_name='category'),
            preserve_default=True,
        ),
    ]
