# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('elephantblog', '0004_auto_20170927_1454'),
        ('page', '0005_auto_20171115_0801'),
    ]

    operations = [
        migrations.AddField(
            model_name='newslistingcontent',
            name='category',
            field=models.ForeignKey(related_name='+', default=1, verbose_name='category', to='elephantblog.CategoryTranslation'),
            preserve_default=False,
        ),
    ]
