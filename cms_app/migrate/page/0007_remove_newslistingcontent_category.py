# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('page', '0006_newslistingcontent_category'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='newslistingcontent',
            name='category',
        ),
    ]
