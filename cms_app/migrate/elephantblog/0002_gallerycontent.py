# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('elephantblog', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='GalleryContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'Image Galleries',
                'db_table': 'elephantblog_entry_gallerycontent',
                'verbose_name': 'Image Gallery',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
    ]
