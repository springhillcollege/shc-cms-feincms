from django.core.management.base import NoArgsCommand
from django.core import mail

class Command(NoArgsCommand):
    help = "Test SMTP config"

    errors = []

    def handle_noargs(self, **options):
        mail.send_mail('Test Subject', 'Here is the test message.', 'noreply@shc.edu', ['chughes@shc.edu'], fail_silently=False)