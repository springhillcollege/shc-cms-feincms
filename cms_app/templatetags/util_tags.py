from django import template
from django.conf import settings
from django.core.cache import cache
from django.db.models import F
from django.core.exceptions import ObjectDoesNotExist
from feincms.module.page.models import Page
from degrees.models import AcademicArea, MMC, Degree
from page_partials.models import PagePartial
import collections

from random import sample
import textile

register = template.Library()


def log(key, msg):
    print "#######################################"
    print "# %s:" % key
    print msg


@register.filter
def pick_attr(iterable, key):
    return [item[key] for item in iterable.values()]


@register.filter
def keyvalue(dict, key):
    try:
        return dict[key]
    except KeyError:
        return ''


@register.filter
def slice0to(value, upto):
    """Use: {% for a in mylist|slice0to:z %}"""
    try:
        return value[0:upto]
    except (ValueError, TypeError):
        return value  # Fail silently.


@register.assignment_tag()
def get_random_testimonials_for_page(page, num=3):
    attributions = []
    items = []
    if page.testimonials.exists():
        for tm in page.testimonials.all().order_by('?'):
            if tm.attribution not in attributions:
                items.append(tm)
                attributions.append(tm.attribution)
        if len(items) > num:
            return sample(items, num)
        return items
    return None


@register.assignment_tag()
def get_acad_area_img_path(acad_area_slug):
    return settings.SHC_CMS["image_paths"]["academic_area_header_image_pattern"] % acad_area_slug


@register.assignment_tag()
def get_even_and_last_items(items):
    num_items = len(items)
    if num_items == 1:
        return (False, items[0])
    if num_items % 2 != 0:
        # we have odd number of items
        return (items[:-1], items[-1])
    else:
        return (items, False)


@register.assignment_tag()
def get_grouped_degrees_by_level(get_acad_level):
    grouped_degrees = []
    areas = AcademicArea.objects.filter(level=get_acad_level)
    for area in areas:
        degrees = Degree.objects.filter(page_ref__active=True, academic_area=area)
        data = {"area": area, "degrees": degrees}
        grouped_degrees.append(data)
    return grouped_degrees


@register.assignment_tag()
def get_degree_by_page_slug(slug):
    try:
        return Degree.objects.get(page_ref__slug=slug)
    except:
        return None


@register.assignment_tag()
def get_degrees_in_area(page, acad_area="self", exclude_page=False):

    majors_cache_secs = 60 * 60  # 1 hour

    page_degree = Degree.objects.get(page_ref__slug=page.slug)
    degrees = Degree.objects.filter(page_ref__active=True)

    # For some reason, this needs to come BEFORE filtering
    # for academic area
    if exclude_page:
        degrees = degrees.exclude(id=page_degree.id)

    if acad_area == "self":
        degrees = degrees.filter(academic_area=page_degree.academic_area)
    elif acad_area:
        degrees = degrees.filter(academic_area=acad_area)

    data = {"area": page_degree.academic_area, "degrees": degrees}

    return data


@register.assignment_tag()
def get_partial_by_blog_category(cats):
    media_map = settings.MEDIA_CONTACT_MAP
    if not media_map:
        return ""

    # find the highest ordered category for which we have a mapped partial
    keys = media_map.keys()
    slugs = [(9999, "__default__")]
    if len(cats):
        for cat in cats:
            this_slug = cat.translations.all()[0].slug
            if this_slug in keys:
                slugs.append((cat.ordering, this_slug))
        slugs.sort(key=lambda x: x[0])
    slug_name = slugs[0][1]

    try:
        me = PagePartial.objects.get(
            slug=media_map.get(slug_name))
        return me.get_rendered_content()
    except ObjectDoesNotExist:
        return ""


@register.assignment_tag()
def get_dept_slug_by_degree(page):
    page_degree = Degree.objects.get(page_ref__slug=page.slug)
    return page_degree.dept_code


@register.filter
def to_textile(txt, head_offset=0):
    return textile.textile(txt, head_offset=head_offset)


@register.filter
def dump(var):
    log(var, dir(var))
