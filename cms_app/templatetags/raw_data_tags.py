# -*- coding: utf-8 -*-
from django import template

register = template.Library()

@register.assignment_tag()
def get_cs_degree_data():
    return [
        {
            "title": "Concentration in Supply Chain Management",
            "link_to": "/academics/continuing-studies/concentration-supply-chain-management/",
            "img_url": "degrees/CS_SupplyChn.jpg",
            "blurb": "The objective of the Supply Chain Management concentration is to equip students with the management skills needed to direct the flow of goods and services within industries.",
        },
        {
            "title": "Concentration in Management and Marketing",
            "link_to": "/academics/continuing-studies/concentration-management-and-marketing/",
            "img_url": "degrees/CS_MgmtMkg.jpg",
            "blurb": "The objective of the Management and Marketing Concentration is to provide students with an understanding of the various aspects of modern organization management and marketing practices.",
        },
        {
            "title": "Concentration in Computer Information Systems (CIS)",
            "link_to": "/academics/continuing-studies/concentration-computer-information-systems-cis/",
            "img_url": "degrees/CS_CIS.jpg",
            "blurb": "The computer information systems concentration equips students with the skills and knowledge needed to allow them to work in organizations that have diverse information processing needs.",
        },
        {
            "title": "Concentration in Sports Management",
            "link_to": "/academics/continuing-studies/concentration-sports-management/",
            "img_url": "degrees/CS_SportsMgmt.jpg",
            "blurb": "The Sports Management concentration combines business management foundations with the ever-growing field of sport. ",
        },
        
    ]
    return degrees

@register.assignment_tag()
def get_nursing_degree_data():
    return [
        {
            "title": "Bachelor of Science in Nursing",
            "link_to": "/academics/freshmen-transfer/majors/nursing/",
            "img_url": "degrees/nursing/ug-nursing.png",
            "blurb": "The Bachelor of Science in Nursing is a four-year undergraduate program that focuses on personalized instruction, hands-on experience and working with medical experts throughout the Gulf Coast region.",
        },
        {
            "title": "RN to BSN",
            "link_to": "http://departments2.shc.edu/nursing/node/1304",
            "img_url": "degrees/nursing/rn-to-bsn.png",
            "blurb": "Spring Hill College offers a Bachelor of Nursing (BSN) degree designed for RNs who hold an associate degree or diploma in nursing to prepare them for leadership in the health care delivery system.  The BSN program combines online courses and clinical integration with local preceptors to allow nurses to complete the degree requirements in their local communities and work settings.",
        },
        {
            "title": "Clinical Nurse Leaders",
            "link_to": "/academics/graduate-students/degrees/master-science-nursing/post-masters-certificate-cnl/",
            "img_url": "degrees/nursing/cnl.png",
            "blurb": "The Post-Master’s Certificate for Clinical Nurse Leader is designed for a registered nurse who has earned a master’s degree in another nursing specialization and wishes to be eligible to take the CNL certification exam.",
        },
        {
            "title": "Master of Science in Nursing (MSN)",
            "link_to": "/academics/graduate-students/degrees/master-science-nursing/",
            "img_url": "degrees/nursing/msn.png",
            "blurb": "Spring Hill College offers a Master of Science in Nursing (MSN) degree program designed to prepare nurses for leadership in the health care delivery system as Clinical Nurse Leaders (CNL). CNLs are advanced generalist clinicians who assume accountability for patient-care outcomes at the point-of-care in any setting where healthcare is delivered.",
        },
        
    ]
    return degrees