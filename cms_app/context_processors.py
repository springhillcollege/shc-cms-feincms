from __future__ import absolute_import, unicode_literals

from django.utils.functional import cached_property

from django.conf import settings

from feincms.module.page.models import Page

from data.menus import menus
# from data.academic_areas import ACADEMIC_AREAS

class Context(object):
    def __init__(self, request):
        self.request = request

    @cached_property
    def meta_navigation(self):
        return Page.objects.in_navigation().filter(
            parent___cached_url='/meta/')

def box(request):
    return {
        'box': Context(request),
    }

def menu_links(request):
	return {
		'shc_links': menus,
	}

def page_vars(request):
    return {
        'debug': settings.DEBUG,
        'test': True,
        'site_name': "Spring Hill College",
        'base_url': "https://www.shc.edu",
    }
