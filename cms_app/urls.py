from __future__ import absolute_import, unicode_literals

from django.conf import settings
from django.conf.urls import url, include, patterns
from django.contrib import admin
from django.views import generic
from degrees.views import degrees_ajax_filter
from cms_theme.views import render_template, xhr_textile, fake_iframe

import forms_builder.forms.urls

from feincms.module.page.sitemap import PageSitemap
# don't need to include lib
from elephantblog.sitemap import EntrySitemap
from general.feeds import LatestEntriesFeed, PeriodicEntriesTitleFeed

pagemap = PageSitemap(max_depth=2, navigation_only=True)
pagemap.protocol = "https"

newsmap = EntrySitemap()
newsmap.protocol = "https"

sitemaps = {'pages': pagemap, 'news': newsmap}

admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
    # url(r'^404/$', generic.TemplateView.as_view(template_name='404.html')),
    url(r'^latest/feed/$', LatestEntriesFeed(), name='news_feed'),
    url(r'^biweekly/feed/$', PeriodicEntriesTitleFeed(), name='periodic_news_feed'),
    url(r'^forms/', include(forms_builder.forms.urls)),
    url(r'^apps/', include('apps.urls')),
    # url(r'^magazine/', include('magazine.urls')),
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap',
        {'sitemaps': sitemaps}
        ),
    # Will serve robots.txt from NGINX
    # url(r'^robots\.txt$',
    #     TemplateView.as_view(template_name='robots.txt', content_type='text/plain')
    # ),
    # (r'^ckeditor/', include('ckeditor.urls')),

    url(r'^degrees_filter$', degrees_ajax_filter, name='degrees_filter'),
    url(r'^profile/', include('profiles.urls')),

    url(r'^textile$', xhr_textile, name='textile_filter'),
    url(r'^fake_iframe$', fake_iframe, name='fake_iframe'),

    url(r'^render_template/(?P<pid>\d+)/(?P<filename>.*)$',
        render_template, name='render_template'),

    url(r'^comments/', include('django.contrib.comments.urls')),
)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns(
        '',
        url(r'^__debug__/', include(debug_toolbar.urls)),
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT}),
    )

    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    urlpatterns += staticfiles_urlpatterns()

urlpatterns += patterns(
    '',
    url(r'', include('feincms.contrib.preview.urls')),
    url(r'', include('feincms.urls')),
)
