from django.db import models
from django.utils.translation import ugettext_lazy as _
from feincms.module.page.models import Page

ACADEMIC_LEVEL_CHOICES = (
		('ug', 'Undergraduate'),
		('grad', 'Graduate'),
		('cs', 'Continuing Studies')
	)

# Create your models here.
class AcademicArea(models.Model):
	level = models.CharField(
		"Academic Level",
		max_length=10,
		choices=ACADEMIC_LEVEL_CHOICES
	)
	title = models.CharField(
		"Title",
		max_length=100
	)
	intro = models.TextField(
		"Intro Blurb",
		max_length=1000,
		blank=True
	)
	image_slug = models.CharField(
		max_length=255,
		blank=True,
		help_text='image must already exist in AWS S3 bucket'
	)
	ordering = models.PositiveIntegerField(
        _('ordering'),
        max_length=6,
        default=0,
        editable=False,
        db_index=True
    )

	def __unicode__(self):
		return "%s: %s" % (self.title, self.level)

	class Meta:
		ordering = ('ordering',)

class MMC(models.Model):
	code = models.CharField(
		max_length=2
		)
	description = models.CharField(
		max_length=20)
	
	def __unicode__(self):
		return "%s: %s" % (self.code, self.description)


class Degree(models.Model):
	academic_area = models.ForeignKey(AcademicArea)
	mmc = models.ManyToManyField(
		MMC,
		blank=True,
		null=True)
	dept_code = models.CharField(
		"Department DB code",
		max_length=5,
		blank=True
	)
	page_ref = models.ForeignKey(Page)

	@property
	def title(self):
		return self.page_ref.content_title

	def __unicode__(self):
	    return "%s [%s]" % (self.title, self.academic_area)

	class Meta():
		ordering=['page_ref__title']


