# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('degrees', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='academicarea',
            options={'ordering': ('ordering',)},
        ),
        migrations.AddField(
            model_name='academicarea',
            name='ordering',
            field=models.PositiveIntegerField(default=0, verbose_name='ordering', max_length=6, editable=False, db_index=True),
            preserve_default=True,
        ),
    ]
