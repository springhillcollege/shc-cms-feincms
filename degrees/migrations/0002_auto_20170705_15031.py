# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def reorder(apps, schema_editor):
    AcademicArea = apps.get_model("degrees", "AcademicArea")
    order = 0
    for item in AcademicArea.objects.all():
        order += 1
        item.ordering = order
        item.save()

class Migration(migrations.Migration):

    dependencies = [
        ('degrees', '0002_auto_20170705_1503'),
    ]

    operations = [
        migrations.RunPython(reorder),
    ]
