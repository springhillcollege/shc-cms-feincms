# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='HomeFeature',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('section', models.CharField(default=(b'home_aud', b'Home Audience Links'), max_length=100, choices=[(b'home_aud', b'Home Audience Links'), (b'home_feat', b'Home Feature Links'), (b'home_banners', b'Home Banners')])),
                ('title', models.CharField(max_length=100)),
                ('content', models.TextField(blank=True)),
                ('image', models.ImageField(help_text=b'audience: no image, feature: 541x541px, banner: 1200x450px', null=True, upload_to=b'features/%Y', blank=True)),
                ('icon_class', models.CharField(help_text=b'Not currently used', max_length=100, blank=True)),
                ('link_to', models.CharField(help_text=b'valid URL or page id. You must include the "http://"" for full URLs', max_length=255)),
                ('_rendered_content', models.TextField(default=b'', editable=False, blank=True)),
                ('active', models.BooleanField(default=True)),
                ('order', models.IntegerField(default=100, max_length=6)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
