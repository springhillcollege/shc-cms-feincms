# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home_features', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homefeature',
            name='link_to',
            field=models.CharField(help_text=b'valid URL or page id. You must include the "http://"" for full URLs', max_length=255, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='homefeature',
            name='section',
            field=models.CharField(default=(b'home_aud', b'Home Audience Links'), max_length=100, choices=[(b'home_aud', b'Home Audience Links'), (b'home_banners', b'Home Banners')]),
            preserve_default=True,
        ),
    ]
