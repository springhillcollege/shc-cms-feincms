# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home_features', '0003_homefeature_show_content_block'),
    ]

    operations = [
        migrations.AddField(
            model_name='homefeature',
            name='image_alt',
            field=models.CharField(default='homepage banner', help_text=b'For accessibility purposes, provide text that will be used in the images alt attribute', max_length=125),
            preserve_default=False,
        ),
    ]
