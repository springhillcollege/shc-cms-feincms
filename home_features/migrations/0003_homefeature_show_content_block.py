# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home_features', '0002_auto_20170424_1524'),
    ]

    operations = [
        migrations.AddField(
            model_name='homefeature',
            name='show_content_block',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
