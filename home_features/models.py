from django.db import models
from feincms.module.page.models import Page
import re
import textile
# Create your models here.

PAGE_ID_URL = re.compile(r'^\d+')

HOME_AUDIENCE = "home_aud"
HOME_BANNERS = "home_banners"

FEATURE_SECTION_CHOICES = (
    (HOME_AUDIENCE, "Home Audience Links"),
    (HOME_BANNERS, "Home Banners")
)


class HomeFeature(models.Model):
    section = models.CharField(
        max_length=100,
        choices=FEATURE_SECTION_CHOICES,
        default=FEATURE_SECTION_CHOICES[0]
    )
    title = models.CharField(
        max_length=100
    )
    content = models.TextField(
        blank=True
    )
    show_content_block = models.BooleanField(
        default=True
    )
    image = models.ImageField(
        upload_to="features/%Y",
        blank=True,
        null=True,
        help_text='audience: no image, feature: 541x541px, banner: 1200x450px'
    )
    image_alt = models.CharField(
        max_length=125,
        blank=False,
        help_text='For accessibility purposes, provide text that will be used in the images alt attribute'
    )
    icon_class = models.CharField(
        max_length=100,
        blank=True,
        help_text='Not currently used'
    )
    link_to = models.CharField(
        max_length=255,
        blank=True,
        help_text='valid URL or page id. You must include the "http://"" for full URLs'
    )
    # _cached_url = models.CharField(
    #     'Cached URL',
    #     max_length=255,
    #     blank=True,
    #     editable=False,
    #     default='',
    #     db_index=True
    #     )
    _rendered_content = models.TextField(
        blank=True,
        editable=False,
        default=''
    )
    active = models.BooleanField(
        default=True
    )
    order = models.IntegerField(
        max_length=6,
        default=100
    )

    def __unicode__(self):
        return "%s" % self.title

    @property
    def url(self):
        try:
            page = Page.objects.get(pk=self.link_to)
        except ValueError:
            # if we don't get a page ref, just return the URL
            return self.link_to
        return page.get_absolute_url()

    @property
    def get_rendered_content(self):
        return self._rendered_content or self.render_content()

    def render_content(self):
        return textile.textile(self.content)

    # def cache_url(self):
    #     url = False
    #     if PAGE_ID_URL.match(self.link_to):
    #         page = Page.objects.get(pk=self.link_to)
    #         url = page.get_absolute_url()
    #     else:
    #         url = self.link_to
    #     return url

    def save(self, *args, **kwargs):
        """
        Overridden save method which updates the ``_rendered_content`` attribute of
        this page.
        """
        # self._cached_url = self.cache_url()
        self._rendered_content = self.render_content()
        super(HomeFeature, self).save(*args, **kwargs)
