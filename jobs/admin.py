from django.contrib import admin
from .models import JobPosting

class JobPostingAdmin(admin.ModelAdmin):
    list_display = ('title', 'type', 'post_date', 'last_date')
    # list_editable = ('title', 'active', 'order')
    list_filter = ('type', 'post_date')
    ordering = ('type', 'title')

# Register your models here.
admin.site.register(JobPosting, JobPostingAdmin)
