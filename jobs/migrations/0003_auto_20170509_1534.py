# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0002_auto_20170509_1351'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jobposting',
            name='post_date',
            field=models.DateTimeField(default=datetime.datetime.now, help_text=b'Postings are sorted reverse chronologically by post date'),
            preserve_default=True,
        ),
    ]
