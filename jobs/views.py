from django.shortcuts import get_object_or_404
from .models import JobPosting

def entry_list(request):
    # Pagination should probably be added here
    return 'jobs/entry_list.html', {
    	'object_list': {
    		'fac': JobPosting.objects.filter(type='fac'),
    		'staff': JobPosting.objects.filter(type='sta')
    		}
    	}

def entry_detail(request, id):
    return 'jobs/entry_detail.html', {'job': get_object_or_404(JobPosting, id=id)}