CONTACT_LINKS = [
	{
		"title":"Contact Us",
		"get_absolute_url": "/admissions/contact-us",
	},
	{
		"title":"Faculty and Staff Directory",
		"get_absolute_url": "http://www.shc.edu/about/directory/",
	},
	{
		"title":"Departmental Websites",
		"get_absolute_url": "http://www.shc.edu/page/departmental-websites",
		"hideOnMobile": True,
	},
	{
		"title":"Faculty Websites",
		"get_absolute_url": "http://www.shc.edu/page/faculty-websites",
		"hideOnMobile": True,
	}
]

SERVICES_LINKS = [
	{
		"title":"Campus Map",
		"get_absolute_url": "http://www.shc.edu/about/map/",
	},
	{
		"title":"BadgerWeb",
		"get_absolute_url": "http://badgerweb.shc.edu/",
	},
	{
		"title":"Campus Email",
		"get_absolute_url": "http://www.shc.edu/page/using-campus-email",
	},
]

OTHER_LINKS = [
	{
		"title":"Privacy Policy",
		"get_absolute_url": "http://www.shc.edu/privacy/",
	}
]

SOCIAL_LINKS = [
	{
		"title":"Like us on Facebook",
		"get_absolute_url": "//www.shc.edu/tracker/facebook-admiss/",
		"icon_class": "has-icon facebook",
	},
	{
		"title":"Follow us on Twitter",
		"get_absolute_url": "//www.shc.edu/tracker/twitter/",
		"icon_class": "has-icon twitter",
	},
	# {
	# 	"title":"Images on Flickr",
	# 	"get_absolute_url": "http://www.shc.edu/tracker/flickr-tour/",
	# 	"icon_class": "has-icon flickr",
	# },
	{
		"title":"Videos on YouTube",
		"get_absolute_url": "//www.shc.edu/tracker/youtube/",
		"icon_class": "has-icon youtube",
	},
	{
		"title":"Snapchat snapcode",
		"get_absolute_url": "//www.shc.edu/tracker/snapchat/",
		"icon_class": "has-icon snapchat",
	},
	{
		"title":"Pictures on Instagram",
		"get_absolute_url": "//www.shc.edu/tracker/instagram/",
		"icon_class": "has-icon instagram",
	},
]
