from __future__ import print_function

import os
import platform
import random

from fabric.api import *
from fabric.contrib.files import exists
from fabric.colors import green, red
from fabric.utils import fastprint

BIN = "/webapps/Envs/shc_cms/bin"

CONFIG = {
    'host': 'deploy@',
    'project': 'cms',
    'branch': 'master',
    'theme_app': 'cms_theme',
}

CONFIG.update({
    'theme': '{theme_app}'.format(**CONFIG),
    'service': 'www-{project}'.format(**CONFIG),
    'folder': 'www/{project}/'.format(**CONFIG),
})


class Settings:
    DATA_HOST = APP_HOST = "ubuntu@ec2-54-208-94-30.compute-1.amazonaws.com:2200"

    DATA_CACHE_PATH = "/webapps/django/shc_cms/apps/data/facstaff"
    EMP_CSV_SRC = "empdir.csv"
    EMP_CSV_FILENAME = "%s/%s" % (DATA_CACHE_PATH, EMP_CSV_SRC)
    FACDEG_CSV_SRC = "facdegree.psv"
    FACDEG_CSV_FILENAME = "%s/%s" % (DATA_CACHE_PATH, FACDEG_CSV_SRC)

    API_BASE_URL = "http://dir-data.shc.edu/api/"

    API_ACCESS_TOKEN = "Token 1ce2d13c0b900d35897fa6bdae992310e3433959"

settings = Settings()
# env.use_ssh_config = True
# env.ssh_config_path = "/home/chughes/.ssh/config"
# key_filename = ['~/.ssh/id_rsa.pub', '~/.ssh/chughes-shc-aws.pem',
#                 '~/.ssh/chughes-shc-cms.pem', '~/.ssh/chughes-shc-aws2.pem']


# env.forward_agent = True
# env.hosts = [CONFIG['host']]

getters = [
    {
        "data_url": "emps/.json?private=1",
        "json_cache": "employees_all_alpha.json",
        "html_source": "apps-temp/dir/private/alpha/",
        "html_cache": "employees_all_alpha.html",
    },
    {
        "data_url": "emps/grouped/.json?hlevel=5",
        "json_cache": "employees_all_dept.json",
        "html_source": "apps-temp/dir/private/dept/",
        "html_cache": "employees_all_dept.html",
    },
    {
        "html_source": "apps-temp/dir/private/qr/",
        "html_cache": "employees_all_qr.html",
    }
]


@task
def cache_private_facstaff_data():
    for getter in getters:
        if getter.get('data_url') and getter.get('json_cache'):
            with cd(settings.DATA_CACHE_PATH):
                wgetter = 'wget --header="Authorization: %s" -O %s/%s %s%s' % (
                    settings.API_ACCESS_TOKEN, settings.DATA_CACHE_PATH, getter['json_cache'], settings.API_BASE_URL, getter['data_url'])
                local(wgetter)


@task
def cache_private_facstaff_pages():
    for getter in getters:
        if getter.get('html_source') and getter.get('html_cache'):
            with cd(settings.DATA_CACHE_PATH):
                wgetter = 'wget --header="Authorization: %s" %s/%s -O %s/printable/%s' % (
                    settings.API_ACCESS_TOKEN, "http://localhost:8080", getter["html_source"], settings.DATA_CACHE_PATH, getter['html_cache'], )
                local(wgetter)


@task
def printable_dirs_to_pdf():
    static_file_root = settings.DATA_CACHE_PATH
    css = [
        "/webapps/django/shc_cms/cms_theme/static/theme/vendor/foundation/css/foundation.min.css",
        "/webapps/django/shc_cms/apps/static/apps/styles/directory-print.css",
    ]
    outdir = "%s/printable/" % settings.DATA_CACHE_PATH
    with cd(outdir):
        with warn_only():
            # use the static files already built by DocPad
            local("/usr/bin/prince %s/printable/employees_all_alpha.html -s %s -o %s/shc-directory_alpha.pdf" %
                  (static_file_root, " -s ".join(css), outdir))
            local("/usr/bin/prince %s/printable/employees_all_dept.html -s %s -o %s/shc-directory_dept.pdf" %
                  (static_file_root, " -s ".join(css), outdir))
            local("/usr/bin/prince %s/printable/employees_all_qr.html -s %s -o %s/shc-directory_qr.pdf" %
                  (static_file_root, " -s ".join(css), outdir))
        print("\nPrintable files generated to %s" % outdir)


@task
def build_print_dirs():
    execute('cache_private_facstaff_data')
    execute('cache_private_facstaff_pages')
    execute('printable_dirs_to_pdf')


def _configure(fn):
    def _dec(string, *args, **kwargs):
        return fn(string.format(**CONFIG), *args, **kwargs)
    return _dec


local = _configure(local)
cd = _configure(cd)
run = _configure(run)


@task
def dev():
    import socket
    from threading import Thread
    jobs = [
        Thread(target=lambda: execute('static')),
        Thread(target=lambda: execute('watch_styles')),
        Thread(target=lambda: execute('runserver')),
    ]
    try:
        socket.create_connection(('localhost', 6379), timeout=0.1).close()
    except socket.error:
        jobs.append(Thread(target=lambda: local('redis-server')))
    [j.start() for j in jobs]
    [j.join() for j in jobs]


@task(alias='ws')
def watch_styles():
    local('cd {theme} && grunt run')


@task(alias='rs')
def runserver(port=8000):
    local('{}/python -Wall manage.py runserver 0.0.0.0:{}'.format(BIN, port))


@task(alias='cc')
def clear_cache():
    local('{}/python manage.py clear_cache'.format(BIN))

@task
def collectstatic():
    local("{}/python manage.py collectstatic --noinput".format(BIN))
    execute('cc')

@task
def static():
    # do all grunt commands
    local("coffee -c {theme}/static/richtext/styles.coffee")
    local("cd {theme} && grunt build".format(**CONFIG))
    execute('collectstatic')

@task
def deploy():
    local("git pull")
    local(". /webapps/Envs/shc_cms/bin/activate")
    execute(static)
    local("sudo supervisorctl restart shc_cms:")
    execute('cc')
