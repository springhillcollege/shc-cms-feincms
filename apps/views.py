from django.views.generic.base import TemplateView
from django.conf import settings
import requests

import sys


def load_from_json(data_uri):
    r = requests.get(data_uri, headers=req_headers)
    return r.json()

data_uri_base = "https://dir-data.shc.edu/api/emps/%s"
templfile_base = "apps/directory/printable-facstaff-directory-%s.html"

data_uris = {
    'alpha': ".json?private=1",
    'dept': "grouped/.json?hlevel=5",
    'qr': ".json?private=1"
}

req_headers = {'Authorization': settings.API_ACCESS_TOKEN}


def authorize(request):
    return request.META.get('HTTP_AUTHORIZATION', None) == settings.API_ACCESS_TOKEN


class PrivateDirectoryList(TemplateView):

    def get_context_data(self, **kwargs):
        context = super(PrivateDirectoryList,
                        self).get_context_data(**kwargs)

        slug = self.kwargs["slug"]
        self.template_name = templfile_base % slug

        data_uri = data_uri_base % data_uris[slug]

        if authorize(self.request):
            context["emps"] = load_from_json(
                data_uri
            )
        else:
            context["error"] = "Not authorized"
        return context
