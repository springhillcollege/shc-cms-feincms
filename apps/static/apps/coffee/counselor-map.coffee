unmunge = (memail) ->
  link = ""
  for i in [0..memail.length] by 2
    chunk = memail[i..i+1]
    link = link + String.fromCharCode(parseInt(chunk, 16))
  # getting an undefined character at the end of every email addy
  # chop it off
  return link.substring 0, link.length-1

initialize = ->
  # template = Handlebars.compile($("#counselor-template").html())
  # intl_template = Handlebars.compile($("#intl-counselor-template").html())

  shc = {
    lat: 30.692826
    lng: -88.135672
    auth_token: {"Authorization": "Token 1ce2d13c0b900d35897fa6bdae992310e3433959"}
    jsonData: ''
  }
  # Create a simple map.
  map = new google.maps.Map document.getElementById('map-canvas'), {
    zoom: 6
    center: shc
  }
  # drop map marker on SHC
  marker = new google.maps.Marker {
    position: shc
    map: map
    title:"Spring Hill College"
  }

  $.ajax('https://dir-data.shc.edu/api/emps/group/COUNS/territories/.json'
        {headers: shc.auth_token}
    ).success (data) ->
        # Load a GeoJSON from the same server as our script.
        map.data.addGeoJson data
        data.intl.email = unmunge(data.intl.memail)
        $(".intl-counselor").html(hbt_apps["../apps/templates/apps/counselors/intl_counselor.handlebars"](data.intl))

  if navigator.geolocation
    navigator.geolocation.getCurrentPosition (position) ->
      initialLocation = new google.maps.LatLng position.coords.latitude, position.coords.longitude
      map.setCenter initialLocation

  # Set initial feature color based on GeoJSON data.
  # Set highlight color if "highlight" property is true
  setFeatureStyle = (feature) ->
    c = feature.getProperty('color')
    o = 0.4
    scolor = "#999"
    sweight = 1
    z = 100
    if feature.getProperty('highlight')
      o = 0.7
      scolor = "#333"
      sweight = 2
      z = 200
    return ({
      fillColor: c
      fillOpacity: o
      strokeColor: scolor
      strokeWeight: sweight
      zIndex: z
    })

  map.data.setStyle setFeatureStyle
  # Set click event for each feature.
  map.data.addListener 'click', (event) ->
    # reset all map features to NOT highlighted
    map.data.forEach (feature) ->
      feature.setProperty 'highlight', false
    # set clicked feature to highlight
    event.feature.setProperty 'highlight', true
    # process context and send to handlebars template
    event.feature.toGeoJson (data) ->
      context = data.properties
      context.employee.email = unmunge(data.properties.employee.memail)
      if $('html').hasClass('touch')
        context.touch = true
      html = hbt_apps["../apps/templates/apps/counselors/counselor.handlebars"](context)
      $('#info-box .modal-content').html(html)
      $('#info-box').foundation('reveal', 'open', {close_on_background_click: true})

$(document).ready ->
  initialize()
