$ ->
    # render group contact info in client-side handlebars template
    if $(".rendered-group-contacts").length
        # console.log hbt_apps["../apps/templates/apps/directory/group-contacts.handlebars"]
        renderTemplateFromAJAX(hbt_apps["../apps/templates/apps/directory/group-contacts.handlebars"], "group_contact_info", "rendered-group-contacts", callback=unmunge_emails)

directoryApp = angular.module('SHCDirectory', []).config ($compileProvider) ->
    # allow linkable URL schemas
    $compileProvider.urlSanitizationWhitelist (/^\s*(http|mailto|tel):/)

directoryApp.factory 'fetcher', ($http, $q) ->
    $http.defaults.headers.common.Authorization = 'Token 1ce2d13c0b900d35897fa6bdae992310e3433959'
    return {
        data: (url) ->
            #create our deferred object.
            deferred = $q.defer()

            #make the call.
            $http.get(url).success (data) ->
                #when data is returned resolve the deferment.
                deferred.resolve(data)
            .error () ->
                #or reject it if there's a problem.
                deferred.reject()

            #return the promise that work will be done.
            deferred.promise
    }

directoryApp.directive 'focusOn', () ->
    return (scope, elem, attr) ->
        scope.$on attr.focusOn, (e) ->
            # console.log 'focusing... ', elem[0]
            elem[0].focus()

directoryApp.directive 'eatClick', () ->
    return (scope, element, attrs) ->
        angular.element(element).bind 'click', (event) ->
            event.preventDefault()

directoryApp.controller('EmployeeCtrl', ($scope, $log, $http, $location, $filter, $timeout, fetcher) ->

    if !String.prototype.trim
      String.prototype.trim = () ->
        this.replace(/^\s+|\s+$/g,'')

    settings = {
        facstaff_search_base: 'https://dir-data.shc.edu/api/emps/search/.json?hlevel=5&q='
        facstaff_by_dept_base: 'https://dir-data.shc.edu/api/emps/group/###/.json?hlevel=5&q='
        department_search_base: 'https://dir-data.shc.edu/api/departments/search/.json?hlevel=5&q='
        base_img_url: '/images/facstaff/'
        facstaff_embed_pagename: '/facstaff-directory-embed.html'
    }

    $scope.strings = {
        no_office: "Office Location"
    }

    $scope.helpers = {}

    $scope.qs = {}

    $scope.employees = {
        list: null
        is_local: false
    }

    $scope.departments = {
        list: null
    }

    $scope.map = {
        embed_code: null
    }

    $scope.search = {
        loading: false
        q: ''
        min_q_chars: 3
        absUrl: null
        in_limbo: false
    }

    $scope.embed = {
        iframe_h: 800
    }

    $scope.employees.orderProp = "last"

    $scope.helpers.is_dept_code = (code) ->
        code_pattern = /^[A-Z]{2,6}$/
        return code_pattern.test(code)

    $scope.helpers.trim_word_by_chars = (text, len) ->
        text.replace(/^(.{len}[^\s]*).*/, "$1")


    $scope.search.fetch = () ->
        # do a timeout loop here
        $timeout.cancel(window.render_timer)
        if $scope.search.valid_q()
            $scope.$emit 'starting_timeout_loop'
            window.render_timer = $timeout ->
                $scope.employees.fetch_data()
                $scope.departments.fetch_data()
            , 500
        else
            $scope.employees.list = []
            $scope.departments.list = []

    $scope.search.valid_q = () ->
        $scope.search.q.length >= $scope.search.min_q_chars

    $scope.search.countdown = () ->
        # countdown from search.min_q_chars to 0 as user enters text in search
        remaining = $scope.search.min_q_chars
        if $scope.search && $scope.search != 'undefined'
            remaining = $scope.search.min_q_chars - $scope.search.q.length
        # remaining
        if remaining == $scope.search.min_q_chars
            return remaining
        remaining + " more"

    $scope.search.get_placeholder = () ->
        if $scope.search.loading then "Loading data..." else "Search first name, last name, position, or department"

    $scope.search.haveResults = () ->
        return $scope.employees.list?.length or $scope.departments.list?.length

    $scope.search.clear = () ->
        $scope.search.q = ''
        $scope.employees.list = []
        $scope.departments.list = []

    $scope.search.get_embed_code = (q, iframe_h) ->
        loc = window.location
        path = loc.pathname
        # bust up the path and add '-embed' to the end of the pagename
        path = path.split('.')
        path[path.length - 2] = path[path.length - 2] + "-embed"
        # rebuild the URL from pieces
        embed_path = loc.protocol + "//" + loc.host + path.join('.') + "?q=" + q
        return '<iframe src="' + embed_path + '" height="' + iframe_h + '" width="99%" style="border:none">' +
            'Your browser does not support the iframe feature, which is required by this web page.' +
            '</iframe>'

    $scope.search.get_search_url = (url, q) ->
        url = url.split(/[\#|\?]/)[0]
        "#{url}?q=#{q}"

    $scope.search.get_link_code = (url, q) ->
        url = $scope.search.get_search_url(url, q)
        "<a href=\"#{ url }\">\n"+
        "Search SHC Faculty and Staff for \"" + q + "\"\n"+
        "</a>"

    $scope.search.setQS = ->
        qs = $location.search()
        if qs.q
            $scope.search.q = qs.q


    $scope.employees.fetch_data = () ->
        $scope.$emit 'data_loading'
        # get employee data from json data file
        fetch_url = settings.facstaff_search_base + $scope.search.q
        fetch = fetcher.data fetch_url
        fetch.then (data) ->
            $scope.employees.list = data
            $scope.error = false
            $scope.$emit 'data_loaded'

    $scope.employees.fetch_by_dept = (code) ->
        $scope.$emit 'data_loading'
        fetch_url = settings.facstaff_by_dept_base.replace("###", code)
        fetch = fetcher.data fetch_url
        fetch.then (data) ->
            $scope.employees.list = data.emps
            $scope.error = false
            $scope.$emit 'data_loaded'

    $scope.employees.toggleDetails = (person) ->
        if !person.showDetail?
            person.showDetail = true
        else
            person.showDetail = not person.showDetail

    $scope.employees.getType = (person) ->
        if person['emp_type'] == 'fac' then "Faculty" else "Staff"

    $scope.employees.getMungedEmail = (person) ->
        # don't decode if we have already done so
        if person.email
            return person.email
        link = ""
        # decode hex-encoded email
        for i in [0..person.memail.length]  by 2
            chunk = person.memail[i..i+1]
            link = link + String.fromCharCode(parseInt(chunk, 16))
        person.email = link
        link

    $scope.employees.phone_link = (phone_number) ->
        if phone_number?
            re = /[-)\ ]/g
            phone_number.replace(re, "")


    $scope.departments.do_search = (code, name) ->
        $scope.search.q = code
        $scope.departments.fetch_data(code)
        $scope.employees.fetch_by_dept(code)

    $scope.departments.fetch_data = (code) ->
        $scope.$emit 'data_loading'
        # get employee data from json data file
        fetch_url = settings.department_search_base + $scope.search.q
        if code
            fetch_url += "&code=#{code}"
        # console.log fetch_url
        fetch = fetcher.data fetch_url
        fetch.then (data) ->
            if not Array.isArray data
                data = [data]
            $scope.departments.list = data
            $scope.error = false
            $scope.$emit 'data_loaded'

    $scope.$on 'data_loading', () ->
        # turn off loading
        $scope.search.loading = true

    $scope.$on 'data_loaded', () ->
        # turn off loading
        $scope.search.loading = false
        $scope.search.in_limbo = false
        # set up link and embed components
        $scope.search.link = $scope.search.get_link_code($scope.search.absUrl, $scope.search.q)
        $scope.search.embed = $scope.search.get_embed_code($scope.search.q, $scope.embed.iframe_h)

    $scope.$on 'starting_timeout_loop', () ->
        $scope.search.in_limbo = true

    # grab the root url
    $scope.search.absUrl = $location.absUrl()
    qs = $location.search()
    $scope.search.setQS()
    if $scope.search.q
        $scope.departments.fetch_data()
        $scope.employees.fetch_data()
    # initialize timer
    window.render_timer = null
    $scope.$broadcast('search_is_ready')
)
