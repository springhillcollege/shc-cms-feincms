mapApp = angular.module('SHCMap', ['ngRoute']).config ($compileProvider) ->

mapApp.factory 'fetcher', ($http, $q) ->
    return {
        details: (url) ->
            #create our deferred object.
            deferred = $q.defer()

            #make the call.
            $http.get(url).success (data) ->
                #when data is returned resolve the deferment.
                deferred.resolve(data)
            .error () ->
                #or reject it if there's a problem.
                deferred.reject()

            #return the promise that work will be done.
            deferred.promise
    }


mapApp.directive 'eatClick', () ->
    return (scope, element, attrs) ->
        angular.element(element).bind 'click', (event) ->
            event.preventDefault()

mapApp.directive 'grabHandle', () ->
    {
        restrict: 'A',
        template: '<div id="map-handle" class="{{visibleOn}}">' +
                    '<span class="icon-bar"></span>' +
                    '<span class="icon-bar"></span>' +
                    '<span class="icon-bar"></span>' +
                    '<span class="icon-bar"></span>' +
                    '<span class="icon-bar"></span>' +
                '</div>',
        link: (scope, elem, attrs) ->
            scope.visibleOn = attrs.visibleOn
            # console.log "Recognized the grab-handle directive usage"
    }

mapApp.controller('MapCtrl', ($scope, $filter, $location, $timeout, fetcher) ->

    $scope.map = {
        map: null # google map reference
        infoWindow: null # info window reference
        locations: [] # all layer data from data file
    }

    $scope.map.options = {
        center: new google.maps.LatLng(30.69338717888936,-88.1370610511553)
        zoom: 18
        mapTypeId: google.maps.MapTypeId.HYBRID
        streetViewControl: false
    }


    $scope.map.infoWindowOptions = {
        maxWidth: 500
    }

    $scope.search = {}
    $scope.search.min_search_str_len = 3

    $scope.map.ready = false
    $scope.map.active_item = null

    $scope.map.beautify = false

    $scope.qs = {}

    # TODO: Use local kml layers in production!!!!!
    $scope.map.base_url = "//www.shc.edu/static"
    $scope.map.kml_base_url = $scope.map.base_url + "/apps/data/map/kml/"
    $scope.map.icon_base_url = $scope.map.base_url + "/apps/images/map/icons/"

    $scope.map.locations = {layers:[], items:[]}
    $scope.map.kml_data = {
        'parking':
            name: "Parking"
            filename: 'Parking.kmz'
            icon:
                url: 'car.png'
        'boundary':
            name: "Boundary"
            filename: "Campus-Boundary.kmz"
    }

    $scope.map.help_examples = [
        {
            name: "Building"
            search_string: "Quinlan"

        }
        {
            name: "Building code"
            search_string: "BL"
        }
        {
            name: "Department"
            search_string: "Athletics"
        }
        #{
        #    name: "Function"
        #    search_string: "food"
        #}
    ]

    $scope.map.initialize = () ->

        $scope.map.fetch_locations()
        $scope.map.map = new google.maps.Map document.getElementById("map-canvas"), $scope.map.options
        $scope.map.infowindow = new google.maps.InfoWindow ( $scope.map.infoWindowOptions )

        # listen for close infowindow and set active item accordingly
        google.maps.event.addListener $scope.map.infowindow, 'closeclick', () ->
            $scope.$apply($scope.map.active_item = null)

        google.maps.event.addListener $scope.map.map, 'zoom_changed', () ->
            $scope.map.show_markers_by_current_zoom_level()

        # $scope.map.directionsDisplay.setMap($scope.map.map);

    $scope.$on 'map_loaded', () ->
        $scope.map.toggle_kml_by_id 'boundary'
        $scope.map.show_markers_by_current_zoom_level()
        $scope.map.ready = true

        qs = $location.search()
        $scope.qs.ref = qs.ref
        $scope.qs.refurl = qs.refurl
        if qs.loc or qs.compact
            item = $scope.map.get_item_by_id(qs.loc)
            item.show_compact = qs.compact == "1"
            if qs.loc
                $scope.map.show_marker_by_item_id(item.id)

    # $scope.map.calc_route = () ->
    #     console.log 'calculating!'
    #     start = $scope.map.start
    #     end = "Spring Hill College, Mobile, Alabama"
    #     request = {
    #         origin: start
    #         destination: end
    #         travelMode: google.maps.TravelMode.DRIVING
    #     }
    #     $scope.map.directionsService.route request, (result, status) ->
    #         if status == google.maps.DirectionsStatus.OK
    #             $scope.map.directionsDisplay.setDirections result


    ########################################
    # map load

    $scope.map.fetch_locations = () ->
        data_url = '/static/apps/data/map/mapdata.json'
        fetch = fetcher.details data_url
        fetch.then (data) ->
            $scope.map.locations = data
            $scope.$emit('map_loaded')


    ########################################
    # map items

    $scope.map.get_markers_by_zoom_level = (rel_zoom_level) ->
        (item for item in $scope.map.locations.items when item.show_on_rel_zoom_level? and item.show_on_rel_zoom_level <= rel_zoom_level)


    $scope.map.show_markers_by_current_zoom_level = () ->
        if $scope.map.beautify
            return
        rel_zoom_level = $scope.map.map.getZoom() - $scope.map.options.zoom
        for item in $scope.map.locations.items
            if item.marker
                $scope.map.hide_marker item.marker
        for item in $scope.map.get_markers_by_zoom_level(rel_zoom_level)
            marker = item.marker or $scope.map.make_marker item
            $scope.map.show_marker marker


    $scope.map.get_item_by_id = (id) ->
        for item in $scope.map.locations.items
            if item['id'] == id
                return item


    $scope.map.set_active_item = (item) ->
        $scope.map.show_marker item.marker
        $scope.map.active_item = item

        content = '<h3>' + item.name + '</h3>'
        if item.description
            content += '<p><em>' + item.description + '</em></p>'
        if item.directory and not item.show_compact
            content += '<div>' + item.directory + '</div>'

        $scope.map.infowindow.setContent content
        $scope.map.infowindow.open $scope.map.map, item.marker


    ########################################
    # map markers

    $scope.map.make_marker = (item) ->
        latlng = item.coordinates.split(",")
        latlng = new google.maps.LatLng latlng[1], latlng[0]
        marker_options = {
            position: latlng
            map: $scope.map.map
            title: item.name
        }

        if item.icon
            image = {
                url: $scope.map.icon_base_url + item.icon.url
            }
            if item.icon?.anchor
                anchor = item.icon.anchor.split(",")
                image.anchor = new google.maps.Point anchor[0], anchor[1]
            marker_options.icon = image

        marker = new google.maps.Marker marker_options
        google.maps.event.addListener marker, 'click', () ->
            $scope.$apply($scope.map.set_active_item item)
        item.marker = marker
        marker


    $scope.map.get_item_by_id = (id) ->
        for item in $scope.map.locations.items
            if item['id'] == id
                return item


    $scope.map.show_marker_by_item_id = (id) ->
        item = $scope.map.get_item_by_id(id)
        marker = item.marker or $scope.map.make_marker item
        $scope.map.show_marker marker
        $scope.map.set_active_item item


    $scope.map.show_marker = (marker) ->
        marker.setMap($scope.map.map)


    $scope.map.hide_marker = (marker) ->
        marker.setMap(null)


    ########################################
    # kml layers

    $scope.map.toggle_kml_by_id = (layer_id) ->
        map = null
        layer = $scope.map.kml_data[layer_id]
        cache_buster = "?cb=" + Math.random()*10000000000000
        layer.kml = layer.kml or new google.maps.KmlLayer $scope.map.kml_base_url + layer.filename + cache_buster, {preserveViewport:true}
        if !layer.visible or !layer.visible?
            map = $scope.map.map
        layer.kml.setMap map
        layer.visible = !layer.visible


    $scope.map.hide_kml_layer = (layer) ->
        if layer.kml?
            layer.kml.setMap null
        layer.visible = false


    ########################################
    # map functions

    $scope.map.clear = () ->
        $scope.map.beautify = !$scope.map.beautify
        if $scope.map.beautify
            #  hide map markers
            for item in $scope.map.locations.items
                if item.marker?
                    $scope.map.hide_marker(item.marker)
            # hide kml layers
            for k,layer of $scope.map.kml_data when k != 'boundary'
                $scope.map.hide_kml_layer layer
            # no active item
            $scope.map.active_item = null
        else
            $scope.map.show_markers_by_current_zoom_level()


    $scope.map.reset = () ->
        $scope.map.map.setCenter($scope.map.options.center)
        $scope.map.map.setZoom($scope.map.options.zoom)


    ########################################
    # search functions

    $scope.map.filterByQuery = (item, fields) ->
        q = String($scope.search.q).toLowerCase()
        search_fields = (v.toLowerCase() for k,v of item when k in fields)
        search_str = search_fields.join(" ")
        search_str.indexOf(q) != -1


    $scope.map.filterAllByQuery = (item) ->
        return $scope.map.filterByQuery item, ['name', 'description', 'directory', 'tags']


     $scope.map.filterBldgCodesByQuery = (item) ->
        return $scope.map.filterByQuery item, ['code']


    $scope.map.filteredResults = () ->
        # allow searching building codes with only 2 char search string
        if $scope.search.q && $scope.search.q.length == 2
            $filter('filter')($scope.map.locations.items, $scope.map.filterBldgCodesByQuery)
        # search everything
        else if $scope.search.q && $scope.search.q.length >= $scope.search.min_search_str_len
            $filter('filter')($scope.map.locations.items, $scope.map.filterAllByQuery)
        else
            false

    $scope.search.clear = () ->
        $scope.search.q = ""

    google.maps.visualRefresh = true
    google.maps.event.addDomListener window, 'load', $scope.map.initialize

)
