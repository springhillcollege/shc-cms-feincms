# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Testimonial',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('person_class', models.CharField(max_length=10, choices=[(b'fac', b'Faculty'), (b'sta', b'Staff'), (b'stu', b'Student'), (b'alum', b'Alumni')])),
                ('title', models.CharField(max_length=100, blank=True)),
                ('attribution', models.CharField(max_length=100, blank=True)),
                ('image', models.ImageField(null=True, upload_to=b'images/people', blank=True)),
                ('content', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
