from elephantblog.models import Entry
from elephantblog.feeds import EntryFeed


class LatestEntriesFeed(EntryFeed):
    link = '/latest/feed/'


class PeriodicEntriesTitleFeed(EntryFeed):
    link = '/biweekly/feed/'

    def items(self):
        import datetime
        end_date = datetime.datetime.now()
        start_date = end_date - datetime.timedelta(days=14)
        return Entry.objects.active().filter(published_on__range=(start_date, end_date)).order_by('-published_on')

    def item_description(self, item):
        return ''
