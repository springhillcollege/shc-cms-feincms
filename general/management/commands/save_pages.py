from django.core.management.base import NoArgsCommand

class Command(NoArgsCommand):
    def handle_noargs(self, **options):
        # now do the things that you want with your models here
        from feincms.module.page.models import Page

        pages = Page.objects.all()
        for page in pages:
            page.save()