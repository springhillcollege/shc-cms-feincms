from django.shortcuts import render_to_response
from profiles.models import Profile

from django.template import RequestContext
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

def profile_content(request, pid=None, slug=None):
    modal = request.GET.get('modal')
    try:
        if slug:
            profile = Profile.objects.get(slug=slug)
        else:
            profile = Profile.objects.get(id=pid)
    except (ObjectDoesNotExist, MultipleObjectsReturned):
        profile = None
    if modal:
        template = 'profiles/profile_modal_content.html'
    else:
        template = 'profiles/profile_detail.html'
    return render_to_response(template, {'request':request, 'profile': profile},  context_instance=RequestContext(request))
