from django.db import models
from django import forms
from feincms.models import Base
from feincms.module.mixins import ContentModelMixin
from django.utils.translation import ugettext_lazy as _
from django.template.loader import render_to_string
from feincms.module.medialibrary.fields import MediaFileForeignKey
from feincms.module.medialibrary.models import MediaFile

class Category(models.Model):
    name = models.CharField(_('name'), max_length=100)
    description = models.CharField(_('description'), max_length=200,
                                   blank=True)
    ordering = models.PositiveIntegerField(_('ordering'), default=0)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ('ordering', 'name')
        verbose_name = _('category')
        verbose_name_plural = _('categories')


class Profile(Base, ContentModelMixin):
    is_active = models.BooleanField(
        _('is active'), default=True, db_index=True)
    is_featured = models.BooleanField(
        _('is featured'), default=False, db_index=True)
    title = models.CharField(_('title'), max_length=100)
    image = MediaFileForeignKey(MediaFile, related_name='+',
                                limit_choices_to={'type': 'image'}, blank=True, null=True)
    blurb = models.CharField(_('blurb'), max_length=250, blank=True)
    slug = models.SlugField(
        _('slug'), unique=True, max_length=100)
    last_changed = models.DateTimeField(
        _('last change'), auto_now=True, editable=False)
    ordering = models.PositiveIntegerField(
        _('ordering'),
        max_length=6,
        default=0,
        editable=False,
        db_index=True
        )

    categories = models.ManyToManyField(
        Category, verbose_name=_('categories'),
        related_name='blogentries', blank=True)

    class Meta:
        ordering = ('ordering',)
        verbose_name = _('profile')
        verbose_name_plural = _('profiles')

    def __str__(self):
        return self.title

    @property
    def subtitle(self):
        return self.content_subtitle.splitlines()

class ProfileContent(models.Model):
    category = models.ForeignKey(Category, blank=True, null=True,
                                 verbose_name=_('category'),
                                 help_text=_('Leave blank to list all categories.'))
    number = models.PositiveIntegerField(
        _('number'), default=0, help_text=_('Leave blank to show all profiles.'))

    class Meta:
        abstract = True
        verbose_name = _('profile content')
        verbose_name_plural = _('profile content')

    @property
    def media(self):
        return forms.Media(
            js=('profiles/scripts.js',)
        )

    def render(self, **kwargs):
        ctx = {'content': self}
        ctx['profiles'] = Profile.objects.all()
        if self.category:
            ctx['profiles'] = ctx['profiles'].filter(categories=self.category)
        if self.number:
            ctx['profiles'] = ctx['profiles'][:self.number]
        return render_to_string('profiles/profiles.html', ctx,
                                context_instance=kwargs.get('context'))
