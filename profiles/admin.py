from django.contrib import admin
from .models import Category, Profile
from adminsortable2.admin import SortableAdminMixin
from feincms.admin import item_editor

class ProfileAdmin(SortableAdminMixin, item_editor.ItemEditor):
    list_display = ('title', 'is_active', 'is_featured')
    list_editable = ('is_active', 'is_featured')
    prepopulated_fields = {
        'slug': ('title',),
    }
    ordering = ('ordering',)
    fieldset_insertion_index = 1
    fieldsets = [
        [None, {
            'fields': [
                ('is_active', 'is_featured'),
                ('title', 'slug'),
                'blurb',
                'categories',
                'image',
            ],
        }],
        item_editor.FEINCMS_CONTENT_FIELDSET,
    ]
    raw_id_fields = ('image',)

admin.site.register(Profile, ProfileAdmin)
admin.site.register(Category)
