# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0002_auto_20170420_1327'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='ordering',
            field=models.PositiveIntegerField(default=0, verbose_name='ordering', max_length=6, editable=False, db_index=True),
            preserve_default=True,
        ),
    ]
