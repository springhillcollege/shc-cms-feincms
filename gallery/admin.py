from django.contrib import admin
from adminsortable2.admin import SortableInlineAdminMixin
from .models import Gallery, GalleryMediaFile

# Register your models here.
class GalleryImageInline(SortableInlineAdminMixin, admin.TabularInline):
    model = GalleryMediaFile
    raw_id_fields = ('mediafile', )
    ordering = ('ordering',)


@admin.register(Gallery)
class GalleryAdmin(admin.ModelAdmin):
    inlines = [
        GalleryImageInline,
    ]
